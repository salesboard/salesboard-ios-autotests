__author__ = 'Darya Tuzova'
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import Config
import SBTests
from appium.webdriver.common.touch_action import TouchAction
import re




class Doc(SBTests.TestCase):
      # in one session 5 documents will be created
      def test_document_create(self):
        driver = self.driver
        time.sleep(15)
        driver.find_element_by_name('START SESSION').click()
        # open model that wasn't published (there was a crash some time ago)
        #SBANDROID-963 Application shouldn't crash if user open presentation from PC that is not published
        time.sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeButton[4]').click()
        time.sleep(2)
        driver.find_element_by_name('Ok').click()
        time.sleep(2)
        #document creation
        for i in range(5):
            driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[1]/XCUIElementTypeButton[1]').click()
            time.sleep(2)
            driver.find_element_by_name('Next').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Ok')))
            driver.find_element_by_name("Ok").click()
            #SBANDROID-1331 Next button is inactive, if local lookup or pdf manager fails
            # added it to workflow
            textfield = driver.find_element_by_xpath("//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTextField[1]")
            textfield.click()
            textfield.send_keys("1")
            time.sleep(2)
            driver.find_element_by_name('Next').click()
            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Done')))
            driver.find_element_by_name('Done').click()
            WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'Ok')))
            driver.find_element_by_name("Ok").click()
            time.sleep(2)
        driver.find_element_by_name("ic menuCue").click()
        driver.find_element_by_xpath(
            '//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]').click()
        time.sleep(2)
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'PitchSessionStop')))
        driver.find_element_by_name('PitchSessionStop').click()

      def test_document_signature(self):
        driver = self.driver
        time.sleep(10)
        driver.find_element_by_name('START SESSION').click()
        #document creation
        for i in range(5):
            driver.find_element_by_name("ic menuCue").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Forms')))
            driver.find_element_by_name('Forms').click()
            time.sleep(1)
            driver.find_element_by_name("SBANDROID-753 CrashSignature").click()
            driver.find_element_by_name('Next').click()
            time.sleep(2)
            driver.find_element_by_name('Signature').click()
            x1,y1=400,380
            x2,y2=400,500
            TouchAction(driver).press(x=x1,y=y1).move_to(x=x2,y=y2).release().perform()
            time.sleep(2)
            driver.find_element_by_name('Signature').click()
            time.sleep(2)
            driver.find_element_by_name('Done').click()
            WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'Ok')))
            driver.find_element_by_name("Ok").click()
            time.sleep(5)
        driver.find_element_by_name("ic menuCue").click()
        driver.find_element_by_xpath(
            '//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]').click()
        time.sleep(2)
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'PitchSessionStop')))
        driver.find_element_by_name('PitchSessionStop').click()

      # issue when 2different form uses two different pdfs with the same name, 1pdf was used for 2 forms
      def test_forms_with_same_pdf_name(self):
        driver = self.driver
        time.sleep(10)
        driver.find_element_by_name("ic menuCue").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Forms')))
        driver.find_element_by_name('Forms').click()
        time.sleep(1)
        driver.find_element_by_name("PDFname Stroom(blue)").click()
        driver.find_element_by_name('Next').click()
        time.sleep(2)
        driver.find_element_by_name('Done').click()
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'Ok')))
        driver.find_element_by_name("Ok").click()
        time.sleep(2)
        driver.find_element_by_name("ic menuCue").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Forms')))
        driver.find_element_by_name('Forms').click()
        time.sleep(1)
        driver.find_element_by_name("PDFname Stroom(red)").click()
        driver.find_element_by_name('Next').click()
        time.sleep(2)
        driver.find_element_by_name('Done').click()
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'Ok')))
        driver.find_element_by_name("Ok").click()


      def test_document_contextval(self):
        driver = self.driver
        WebDriverWait(driver,10).until(EC.presence_of_element_located((By.NAME, 'TestContextValue Binckhorstlaan 38 A 2516BE, s-Gravenhage')))
        driver.find_element_by_name('TestContextValue Binckhorstlaan 38 A 2516BE, s-Gravenhage').click()
        time.sleep(2)
        driver.find_element_by_name('START SESSION from this lead').click()
        #document creation
        time.sleep(1)
        driver.find_element_by_name("ic menuCue").click()
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Forms')))
        driver.find_element_by_name('Forms').click()
        time.sleep(1)
        driver.find_element_by_name("Context Value").click()
        time.sleep(1)
        #have to read all values
        fields = driver.find_elements_by_xpath('//XCUIElementTypeTextField[1]')
        print "Here is field values"
        for i in fields:
            values = i.get_attribute("value")
            print values
            if not values:
               print "No contaxt values were set. CHECK PROBLEM"
               assert False
        #check checkbox
        driver.find_element_by_name('Ja').click()
        # select Nee on radiobutton
        driver.find_element_by_name('Nee').click()
        #select value from combox
        driver.find_element_by_name('Options').click()
        time.sleep(2)
        driver.find_element_by_name('Option2').click()
        #select date 02/02/2015
        driver.find_element_by_name('Day').click()
        time.sleep(2)
        driver.find_element_by_name('5').click()
        driver.find_element_by_name('Month').click()
        time.sleep(2)
        driver.find_element_by_name('5').click()
        driver.find_element_by_name('Year').click()
        time.sleep(2)
        driver.find_element_by_name('2015').click()
        time.sleep(1)
        #create document
        driver.find_element_by_name('Next').click()
        time.sleep(2)
        driver.find_element_by_name('Done').click()
        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.NAME, 'Ok')))
        driver.find_element_by_name("Ok").click()
        time.sleep(2)
        driver.find_element_by_name("ic menuCue").click()
        driver.find_element_by_xpath(
            '//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]').click()
        time.sleep(2)
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'PitchSessionStop')))
        driver.find_element_by_name('PitchSessionStop').click()







      def test_document_formvalues(self):
            driver = self.driver
            WebDriverWait(driver, 40).until(
                EC.presence_of_element_located((By.XPATH, "//*[@text='Binckhorstlaan 38 A']")))
            driver.find_element_by_xpath("//*[@text='Binckhorstlaan 38 A']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'start_session')))
            driver.find_element_by_id('start_session').click()
            time.sleep(2)
            # click button on PC with COntext Value form
            driver.find_element_by_xpath('//android.view.View[5]').click()
            time.sleep(2)
            # SBCLIENT-328 In session, when jumping from filled in form to other model, data in form gets lost
            driver.press_keycode(4)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "session_view")))
            driver.find_element_by_id('session_view').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Team A']")))
            driver.find_element_by_xpath("//*[@text='Team A']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.view.View[6]')))
            driver.find_element_by_xpath('//android.view.View[6]').click()
            driver.press_keycode(4)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "session_view")))
            driver.find_element_by_id('session_view').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Team A']")))
            driver.find_element_by_xpath("//*[@text='Team A']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.view.View[5]')))
            driver.find_element_by_xpath('//android.view.View[5]').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "check_box")))
            # fill in form
            # check checkbox
            driver.find_element_by_id('check_box').click()
            WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, '//android.widget.RadioButton[2]')))
            # select Nee on radiobutton
            driver.find_element_by_xpath('//android.widget.RadioButton[2]').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Options']")))
            # select value from combox
            driver.find_element_by_xpath("//*[@text='Options']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Option2']")))
            driver.find_element_by_xpath("//*[@text='Option2']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Day']")))
            # select date 03/03/1923
            driver.find_element_by_xpath("//*[@text='Day']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='03']")))
            driver.find_element_by_xpath("//*[@text='03']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Month']")))
            driver.find_element_by_xpath("//*[@text='Month']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='March']")))
            driver.find_element_by_xpath("//*[@text='March']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Year']")))
            driver.find_element_by_xpath("//*[@text='Year']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='1923']")))
            driver.find_element_by_xpath("//*[@text='1923']").click()
            time.sleep(2)
            # Go back on PC
            driver.press_keycode(4)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "session_view")))
            driver.find_element_by_id('session_view').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Team A']")))
            driver.find_element_by_xpath("//*[@text='Team A']").click()
            # click on form Values check in session
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//android.view.View[6]')))
            driver.find_element_by_xpath('//android.view.View[6]').click()
            # check checkbox
            driver.find_element_by_id('check_box').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Dutch']")))
            driver.find_element_by_xpath("//*[@text='Dutch']").click()
            # select Nee on radiobutton
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Options']")))
            # select value from combox
            driver.find_element_by_xpath("//*[@text='Options']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Option1']")))
            driver.find_element_by_xpath("//*[@text='Option1']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Day']")))
            # select date 03/03/1923
            driver.find_element_by_xpath("//*[@text='Day']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='06']")))
            driver.find_element_by_xpath("//*[@text='06']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Month']")))
            driver.find_element_by_xpath("//*[@text='Month']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='March']")))
            driver.find_element_by_xpath("//*[@text='March']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Year']")))
            driver.find_element_by_xpath("//*[@text='Year']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='1925']")))
            driver.find_element_by_xpath("//*[@text='1925']").click()
            time.sleep(2)
            el = driver.find_element_by_xpath('//android.widget.EditText[1]')
            TouchAction(driver).tap(el).perform()
            driver.find_element_by_xpath('//android.widget.EditText[1]').send_keys("Test message in the text field")
            time.sleep(2)
            driver.press_keycode(4)
            time.sleep(2)
            el = driver.find_element_by_xpath('//android.widget.EditText[2]')
            TouchAction(driver).tap(el).perform()
            driver.find_element_by_xpath('//android.widget.EditText[2]').send_keys("Text Area message")
            time.sleep(2)
            driver.press_keycode(4)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='Postcode']")))
            driver.find_element_by_xpath("//*[@text='Postcode']").click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, "//*[@text='4107NA']")))
            driver.find_element_by_xpath("//*[@text='4107NA']").click()
            # switch forms
            driver.press_keycode(4)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "session_view")))
            driver.find_element_by_id('session_view').click()
            time.sleep(5)
            driver.find_element_by_xpath("//*[@text='Context Value']").click()
            # check that everything stays
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "check_box")))
            checkbox = driver.find_element_by_id("check_box").get_attribute("checked")
            print checkbox
            assert checkbox == 'true'
            combo = driver.find_element_by_xpath("//*[@text='Nee']").get_attribute("checked")
            print  combo
            assert combo == 'true'
            Labels1 = ['Day', 'Month', 'Year', 'Options']
            all_texts1 = driver.find_elements_by_xpath("//android.widget.TextView")
            for item in all_texts1:
                print item.get_attribute("text")
                assert item.get_attribute("text") not in Labels1
            time.sleep(2)
            driver.press_keycode(4)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "session_view")))
            driver.find_element_by_id('session_view').click()
            WebDriverWait(driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//*[@text='Values check in session']")))
            driver.find_element_by_xpath("//*[@text='Values check in session']").click()
            # check that everything stays
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "check_box")))
            checkbox = driver.find_element_by_id("check_box").get_attribute("checked")
            print checkbox
            assert checkbox == 'true'
            combo = driver.find_element_by_xpath("//*[@text='Dutch']").get_attribute("checked")
            print  combo
            assert combo == 'true'
            Labels = ['Day', 'Month', 'Year', 'Options', 'Postcode']
            all_texts1 = driver.find_elements_by_xpath("//android.widget.TextView")
            for item in all_texts1:
                print item.get_attribute("text")
                assert item.get_attribute("text") not in Labels
            # stop session
            time.sleep(2)
            driver.press_keycode(4)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "session_view")))
            driver.find_element_by_id('session_view').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "close_preview")))
            driver.find_element_by_id('close_preview').click()
            time.sleep(1)