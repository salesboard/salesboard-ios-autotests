__author__ = 'ricardo'
import os

dir = os.path.dirname(__file__)
dataurl = os.path.join(dir,'TestData')

sbclient = 'tablet'
username = 'user'
password = '1234'
deviceName = 'iPad Air'
platformVersion = '9.3'
#build = "2.2.6.202"

#tablet user "user" has 2 teams assigned with different types of content

isRemote = 0


def getTarget():
    if isRemote:
        return 'remotely'
    else:
        return 'locally'
