__author__ = 'Darya Tuzova'
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time
import Config
import SBTests
from appium.webdriver.common.touch_action import TouchAction




class Settings(SBTests.TestCase):
      # in one session 5 documents will be created
      def test_clear_content_without_session (self):
          driver = self.driver
          time.sleep(2)
          driver.find_element_by_name("ic menuCue").click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Settings')))
          driver.find_element_by_name("Settings").click()
          time.sleep(1)
          driver.find_element_by_xpath("//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTable[2]/XCUIElementTypeCell[3]/XCUIElementTypeButton[1]").click()
          time.sleep(1)
          driver.find_element_by_name('Clear').click()



      def test_clear_content_in_session (self):
          driver = self.driver
          time.sleep(30)
          driver.find_element_by_name('START SESSION').click()
          driver.find_element_by_name("ic menuCue").click()
          WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Settings')))
          driver.find_element_by_name("Settings").click()
          time.sleep(1)
          driver.find_element_by_xpath("//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTable[2]/XCUIElementTypeCell[3]/XCUIElementTypeButton[1]").click()
          time.sleep(1)
          driver.find_element_by_name('Clear').click()
