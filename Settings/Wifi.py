__author__ = 'Darya Tuzova'
__author__ = 'Darya Tuzova'
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from appium.webdriver.common.touch_action import TouchAction
import time
import Config
import SBTests
from selenium.webdriver.common.keys import Keys


class OfflineModeTest(SBTests.TestCase):
      def test_offline_mode(self):
        driver = self.driver
        time.sleep(5)
        el = driver.find_element_by_name('Boekhorst 32 2512CR, Den Haag')
        x2, y2 = 0, 200
        TouchAction(driver).press(el).move_to(x=x2, y=y2).release().perform()
        time.sleep(5)
        #driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeSearchField[1]').click()
        time.sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeSearchField[1]').send_keys("H"+Keys.SHIFT+Keys.COMMAND)
        time.sleep(2)
        x1, y1 = 200, 200
        x2, y2 = 600, 200
        TouchAction(driver).press(x=x1, y=y1).move_to(x=x2, y=y2).release().perform()
        #time.sleep(2)
        driver.find_element_by_name("Setting").click()
        #driver.find_element_by_name("Wi-Fi").click()
        time.sleep(2)
        driver.find_element_by_name("Airplane Mode").click()
        #pannel = driver.find_element_by_xpath("//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[3]/XCUIElementTypeScrollView[1]")
        #x1, y1 = 0, 0
        #x2, y2 = 0, 100
        #TouchAction(driver).press(pannel, x=x1, y=y1).move_to(x=x2, y=y2).release().perform()
        #time.sleep(2)




      def test_get_online(self):
        driver = self.driver
        time.sleep(2)
        driver.press_keycode(3)
        time.sleep(5)
        driver.find_element_by_xpath("//*[@text='Settings']").click()
        time.sleep(5)
        driver.find_element_by_xpath("//*[contains(@text,'More')]").click()
        time.sleep(5)
        driver.find_element_by_xpath("//*[@text='Airplane mode']").click()
        #show SB in recent apps only android 5.0 and 5.1.
        #Android 4.4 doesn't show SB in recent
        driver.press_keycode(187)
        time.sleep(5)
        driver.find_element_by_xpath("//*[@text='Salesboard']").click()
        time.sleep(2)
