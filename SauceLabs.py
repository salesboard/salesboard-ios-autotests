__author__ = 'ricardo'
import Config
import sys
import os
from appium import webdriver
from sauceclient import SauceClient
import TestResults

# it's best to remove the hardcoded defaults and always get these values
# from environment variables
USERNAME = os.environ.get('SAUCE_USERNAME', "swsalesboard")
ACCESS_KEY = os.environ.get('SAUCE_ACCESS_KEY', "0bb3efc3-c031-44a7-b42b-26857a1d9601")


sauce = SauceClient(USERNAME, ACCESS_KEY)

def RemoteDriver(self):

    caps = {}
    caps['platformVersion'] =Config.platformVersion
    caps['deviceName'] =Config.deviceName
    caps['app'] = 'https://versions.salesboard.biz/latest/ios/'+Config.build+'/SB%20Enterprise_sim.zip'
    caps['platformName'] = "iOS"
    caps['deviceType'] = 'tablet'
    caps['deviceOrientation'] = 'landscape'
    caps['automationName'] = 'XCUITest'
    caps['appiumVersion'] = '1.6.3'
    caps['autoAcceptAlerts'] = True
    caps['autoDismissAlerts'] = True
    #caps['fullReset'] = True

    #run
    self.desired_capabilities = caps
    # http://saucelabs.com/docs/additional-config
    self.desired_capabilities['tags'] = ['cloud', 'testing']
    self.desired_capabilities['custom-data'] = { "release": "1.0", "commit": "0k392a9dkjr", "staging": "true" }#TODO
    self.desired_capabilities['idle-timeout'] = 90 #seconds

    self.desired_capabilities['name'] = self.id()

    sauce_url = "http://%s:%s@ondemand.saucelabs.com:80/wd/hub"
    self.driver = webdriver.Remote(
        desired_capabilities=self.desired_capabilities,
        command_executor=sauce_url % (USERNAME, ACCESS_KEY)
    )
    self.driver.implicitly_wait(30)
    return self.driver


def tearDown(self):

    print >>sys.stderr, "Link to your job: https://saucelabs.com/jobs/%s" % self.driver.session_id
    try:
        if TestResults.errors == 0: # if sys.exc_info() == (None, None, None):
            sauce.jobs.update_job(self.driver.session_id, passed=True)
        else:
            sauce.jobs.update_job(self.driver.session_id, passed=False)
    finally:
        self.driver.quit()

def updateSessionName(self):
    sauce.jobs.update_job(self.driver.sess, passed=False)
