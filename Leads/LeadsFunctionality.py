__author__ = 'Darya Tuzova'
import time
import SBTests
import datetime
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException,TimeoutException
from selenium.webdriver.common.keys import Keys
from appium.webdriver.common.touch_action import TouchAction
import re

class Leads(SBTests.TestCase):

    def test_lead_search(self):
        driver = self.driver
        time.sleep(5)
        el = driver.find_element_by_name('Sharewire3 Binckhorstlaan 38 2516BE, s-Gravenhage')
        x2, y2 = 0, 200
        TouchAction(driver).press(el).move_to(x=x2, y=y2).release().perform()
        time.sleep(5)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeSearchField[1]').click()
        time.sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeSearchField[1]').send_keys("Sharewire")
        driver.find_element_by_name('Clear text').click()

    #SBANDROID-980 Lead search: app crashed if ' is filled in as search parameter
    def test_lead_search_spesial_character(self):
        driver = self.driver
        time.sleep(1)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeSearchField[1]').send_keys(" ' % \" ")
        time.sleep(2)
        driver.find_element_by_name('Clear text').click()

    def test_grouping(self): ## sorting and grouping is not possible because dropdown list isn't visible in UI
        driver = self.driver
        time.sleep(2)
        driver.find_element_by_name('None').click()
        time.sleep(2)
        driver.find_element_by_name("Postcode").click()


    def test_sorting(self):
        driver = self.driver
        time.sleep(2)
        driver.find_element_by_name('Distance').click()
        time.sleep(2)
        driver.find_element_by_name("Street").click()


    def test_lead_refresh(self):
        driver = self.driver
        time.sleep(2)
        for i in range(10):
            driver.find_element_by_name('ic reload').click()
            print i


    def test_start_session(self):
        driver = self.driver
        driver.find_element_by_name('START SESSION').click()
        driver.find_element_by_name("ic menuCue").click()
        time.sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]').click()
        time.sleep(2)
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'PitchSessionStop')))
        driver.find_element_by_name('PitchSessionStop').click()


    #often crash on ios during navigation between dashboards
    def test_dashboard_navigation(self):
        driver = self.driver
        for i in range(10):
            driver.find_element_by_name('ic arrowRight darkGrey').click()
            driver.find_element_by_id('ic arrowRight darkGrey').click()
            driver.find_element_by_id('ic arrowLeft darkGrey').click()
            driver.find_element_by_id('ic arrowLeft darkGrey').click()
            print i

    def test_lead_datalayer(self):
        driver = self.driver
        driver.find_element_by_name('Sharewire3 Binckhorstlaan 38 2516BE, s-Gravenhage').click()
        time.sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeCollectionView[1]/XCUIElementTypeCell[1]').click()


    def test_lead_field_edit(self):
        driver = self.driver
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'ic info')))
        driver.find_element_by_name('ic info').click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'info')))
        driver.find_element_by_name('info').click()
        date = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
        print date
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[2]/XCUIElementTypeTextView[1]').clear()
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[2]/XCUIElementTypeTextView[1]').send_keys(date)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[2]/XCUIElementTypeTextView[1]').send_keys(Keys.RETURN)

    def test_lead_remark(self):
        driver = self.driver
        driver.find_element_by_name('ic remark').click()
        time.sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').click()
        #user   time.sleep(2)
        date = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
        print date
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').clear()
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').send_keys("Remark at "+date)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').send_keys(Keys.RETURN)

    def test_close_details(self):
        driver = self.driver
        driver.find_element_by_name('ic back').click()
        time.sleep(1)

    def test_lead_offline_remark(self):
        driver = self.driver
        driver.find_element_by_name('ic remark').click()
        time.sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').click()
        date = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
        print date
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').clear()
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').send_keys("OFFLINE remark at " + date)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]').send_keys(Keys.RETURN)

    def test_offline_lead_field_edit(self):
        driver = self.driver
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'ic info')))
        driver.find_element_by_name('ic info').click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'info')))
        driver.find_element_by_name('info').click()
        date = str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M'))
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[2]/XCUIElementTypeTextView[1]').clear()
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[2]/XCUIElementTypeTextView[1]').send_keys("Offline lead edit"+date)
        driver.find_element_by_xpath('//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[2]/XCUIElementTypeTextView[1]').send_keys(Keys.RETURN)










