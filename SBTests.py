__author__ = 'Darya Tuzova'

import unittest
import sys
import os
import Config
import SauceLabs
from appium import webdriver
import TestResults

desired_capabilities = None
DRIVER = None


def getOrCreateWebdriver(self):
    global DRIVER
    global desired_capabilities

    desired_caps = {}
    desired_caps['platformName'] = 'iOS'
    desired_caps['platformVersion'] = Config.platformVersion
    desired_caps['deviceName'] = Config.deviceName
    desired_caps['app'] = 'https://versions.salesboard.biz/latest/ios/'+Config.build+'/SB%20Enterprise_sim.zip'
    #desired_caps['app'] = 'https://versions.salesboard.biz/latest/ios/'+Config.build+'/SB_Debug.ipa'
    #desired_caps['udid']='0de285aeaab7f30708c8212f71793d341b90735d'
    desired_caps['bundleId'] = 'biz.salesboard.Salesboard'
    desired_caps['autoAcceptAlerts'] = True
    desired_caps['automationName'] = 'XCUITest'
    #desired_caps['deviceOrientation'] = 'landscape'
    desired_caps['autoDismissAlerts'] = True
    desired_caps['fullReset'] = True


    #desired_caps['deviceOrientation'] = 'landscape'
    #9639d1ea0b162380394633d0e6b353f1d704d2d0 ipad pro
    #9bc0e1a9a0da794680a59d4edf2dbff9a8a535cc ipad3 9.3
    #05b6d65f5bb1646f2442989a91321c80f0e17859 air 9.3

    if Config.isRemote:
        print "Executing Remote tests with SauceLabs..."
        DRIVER = DRIVER or SauceLabs.RemoteDriver(self);
    else:
        print "Executing Local tests with APPIUM..."
        DRIVER = DRIVER or webdriver.Remote('http://localhost:4777/wd/hub', desired_caps)

    return DRIVER


class TestCase(unittest.TestCase):

    def setUp(self):
        self.driver = getOrCreateWebdriver(self)

    def tearDown(self):
        if sys.exc_info() != (None, None, None):
            TestResults.errors += 1;
            TestResults.total_errors += 1;

    def finish(self):
        print >>sys.stderr, '\n%s' % TestResults.errors, " errors at this test suite"

        TestResults.errors = 0;

        if Config.isRemote:
            print "Remote tearDown..."
            SauceLabs.tearDown(self);

        else:
            print "Local tearDown ..."

        self.driver = None

        global DRIVER
        DRIVER = None