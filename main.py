__author__ = 'Darya Tuzova'
# Android environment
import unittest
import os
import time
import HTMLTestRunner
from Login.LoginApp import *
from Leads.LeadsFunctionality import *
import TestResults
import sys
import getopt
from DocumentsGeneration.CreateDoc import *
from Settings.Settings import *
from Settings.Wifi import *
from DocumentsGeneration.GoToDoc import *


def execute():
    print >>sys.stderr, '\nsbclient: %s  (running %s)' % (Config.sbclient, Config.getTarget())
    Config.url = 'https://%s.salesboard.biz'% (Config.sbclient)
    print >>sys.stderr, '\n%s' % (Config.url)

    dir = os.path.dirname(__file__)

    suite1 = unittest.TestSuite()
    suite1.addTest((LoginTest('test_login_space_in_client_name')))
    suite1.addTest((LoginTest('test_incorrect_credentials')))
    suite1.addTest((LoginTest('test_login_demo_account')))
    suite1.addTest((LoginTest('test_logout')))
    ####suite1.addTest((LoginTest('test_forgot_password'))) #need names or ids for elements on forgot password screen
    suite1.addTest((LoginTest('test_login_wizzard_teamB')))
    #lead changes
    suite1.addTest((Leads('test_lead_search')))
    suite1.addTest((Leads('test_lead_search_spesial_character')))
    suite1.addTest((Leads('test_lead_refresh')))
    suite1.addTest((Leads('test_start_session')))
    suite1.addTest((Leads('test_lead_datalayer')))
    suite1.addTest((Leads('test_lead_field_edit')))
    suite1.addTest((Leads('test_lead_remark')))       #can't confirm remark
    suite1.addTest((Leads('test_close_details')))
    suite1.addTest((Doc('test_document_contextval')))  #can't fill in day and nonth in date picker
    #navigation withing Dashboards
    suite1.addTest((Leads('test_grouping')))
    suite1.addTest((Leads('test_sorting')))
    suite1.addTest((Leads('test_dashboard_navigation')))
    suite1.addTest((LoginTest('test_logout_english')))
    suite1.addTest((LoginTest('test_login_wizzard_teamA')))
    suite1.addTest((Doc('test_forms_with_same_pdf_name')))
    suite1.addTest((Doc('test_document_create')))
    suite1.addTest((Doc('test_document_signature')))
    suite1.addTest((Settings('test_clear_content_without_session')))
    suite1.addTest((Settings('test_clear_content_in_session')))
    ###suite1.addTest((Doc('test_document_formvalues')))  # SBCLIENT-328
    suite1.addTest((LoginTest('test_logout_english')))
    #Offline mode tests
    #suite1.addTest((OfflineModeTest('test_offline_mode')))
    # suite1.addTest((LoginTest('test_login_wizzard_teamA')))
    # suite1.addTest((Doc('test_forms_with_same_pdf_name')))
    # suite1.addTest((Leads('test_start_session')))
    # suite1.addTest((Leads('test_lead_datalayer')))
    # suite1.addTest((Leads('test_offline_lead_field_edit')))
    # suite1.addTest((Leads('test_lead_offline_remark')))
    # suite1.addTest((Leads('test_close_details')))
    # suite1.addTest((LoginTest('test_logout')))
    # # get back online and login
    # suite1.addTest((OfflineModeTest('test_get_online')))
    # suite1.addTest((LoginTest('test_login_wizzard_teamA')))
    # suite1.addTest((DocDahboard('test_go_to_doc_dashboard')))
    #suite1.addTest((LoginTest('test_logout')))
    suite1.addTest((LoginTest('finish')))#To terminate connection, Driver will have to be re-created

    #run and make report
    dateTimeStamp = time.strftime('%Y_%m_%d_%H_%M_%S')
    outdir = os.path.join(dir,'Reports')
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    fileurl = os.path.join(outdir,"TestReport_"+dateTimeStamp+".html")
    buf = file(fileurl,'wb')
    runner = HTMLTestRunner.HTMLTestRunner(stream=buf, title='Test_Result', description='Result of tests')
    runner.run(suite1)



    if TestResults.total_errors != 0:
        print >>sys.stderr, "Found %s errors in total, tests did not pass" % TestResults.total_errors
        sys.exit(2)
    else:
        print >>sys.stderr, "All good, no errors found."


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def main(argv=None):
    if argv is None:
        argv = sys.argv
    try:
        try:
            opts, args = getopt.getopt(argv[1:], "ho:c:d:p:b:rl", ["help","output=","client=","deviceName=","platformVersion=","buildNumber","remote","local"])
        except getopt.error, msg:
            raise Usage(msg)


        if (len(opts)==0):
            raise Usage('Please provide client name')

        output = None
        verbose = False
        for o, a in opts:
            if o == "-v":
                verbose = True
            elif o in ("-h", "--help"):
                Usage()
                sys.exit()
            elif o in ("-o", "--output"):
                output = a
            elif o in ("-c", "--client"):
                Config.sbclient = a
            elif o in ("-d", "--deviceName"):
                Config.deviceName = a
            elif o in ("-p", "--platformVersion"):
                Config.platformVersion = a
            elif o in ("-b", "--buildNumber"):
                Config.build = a
            elif o in ("-r", "--remote"):
                Config.isRemote = 1
            elif o in ("-l", "--local"):
                Config.isRemote = 0
            else:
                assert False, "unhandled option"

        execute()

    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, ""
        print >>sys.stderr, "-c | --client <client name>    Sets the client name"
        print >> sys.stderr, "-d | --client <client name>    Sets the device name"
        print >> sys.stderr, "-p | --client <client name>    Sets the platform version"
        print >> sys.stderr, "-b | --client <client name>    Sets the build number"
        print >>sys.stderr, "-r | --remote                  It runs tests at SauceLabs servers"
        print >>sys.stderr, "-l | --local                   It runs tests locally"
        print >>sys.stderr, ""
        print >>sys.stderr, "e.g.: python Main.py -c stage --remote"
        print >>sys.stderr, "e.g.: python Main.py -c alpha --local"
        return 2

if __name__ == "__main__":
    sys.exit(main())




