__author__ = 'Darya Tuzova'
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException,TimeoutException
import time
import Config
import SBTests

class LoginTest(SBTests.TestCase):
      # there was a crash if space was entered to the client name
      def test_login_space_in_client_name(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Username')))
        driver.find_element_by_name("Username").send_keys(Config.username)
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Password')))
        driver.find_element_by_name("Password").send_keys(Config.password)
        driver.find_element_by_name("Client").clear()
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Client')))
        driver.find_element_by_name("Client").send_keys(" "+Config.sbclient+" zzz")
        driver.find_element_by_xpath("//XCUIElementTypeOther[2]/XCUIElementTypeButton[1]").click()
        try:
          WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Allow')))
          driver.find_element_by_name("Allow").click()
        except TimeoutException:
          pass
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Ok')))
        driver.find_element_by_name("Ok").click()




      def test_incorrect_credentials(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Username')))
        driver.find_element_by_name("Username").clear()
        driver.find_element_by_name("Username").send_keys("test")
        driver.find_element_by_name("Password").clear()
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Password')))
        driver.find_element_by_name("Password").send_keys("qwerty")
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Client')))
        driver.find_element_by_name("Client").clear()
        driver.find_element_by_name("Client").send_keys(Config.sbclient)
        #WebDriverWait(driver, 15).until(EC.element_to_be_clickable((By.XPATH, '//UIAButton[1]')))
        driver.find_element_by_xpath("//XCUIElementTypeOther[2]/XCUIElementTypeButton[1]").click()
        try:
          WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Allow')))
          driver.find_element_by_name("Allow").click()
        except TimeoutException:
          pass
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Ok')))
        driver.find_element_by_name("Ok").click()



      def test_login_demo_account(self):
        driver = self.driver
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Login at Demo account')))
        driver.find_element_by_name('Login at Demo account').click()
        try:
          WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Allow')))
          driver.find_element_by_name("Allow").click()
        except TimeoutException:
          pass
        #Select Dutch Language
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, '1. Select Language')))
        driver.find_element_by_xpath("//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]").click()
        #driver.find_element_by_name("DropDownArrow").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Nederlands')))
        driver.find_element_by_name("Nederlands").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Volgende')))
        driver.find_element_by_name("Volgende").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, '2. Gebruikersgegevens bevestigen')))
        driver.find_element_by_xpath("//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]").click()
        #Select team
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, '3. Kies team')))
        driver.find_element_by_name("Select team").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Salesboard')))
        driver.find_element_by_name("Salesboard").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Klaar')))
        driver.find_element_by_name("Klaar").click()


      def test_forgot_password(self):
        driver = self.driver
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Wachtwoord vergeten?')))
        driver.find_element_by_name('Wachtwoord vergeten?').click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Reset wachtwoord')))
        driver.find_element_by_xpath('//*[text()="Gebruikersnaam"]').send_keys(Config.username)
        driver.find_element_by_name('Verstuur email').click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Ok')))
        driver.find_element_by_name("Ok").click()


      # team B and Team A have to be created
      def test_login_wizzard_teamB(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Gebruikersnaam')))
        driver.find_element_by_name("Gebruikersnaam").clear()
        driver.find_element_by_name("Gebruikersnaam").send_keys(Config.username)
        driver.find_element_by_name("Wachtwoord").clear()
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Wachtwoord')))
        driver.find_element_by_name("Wachtwoord").send_keys(Config.password)
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Klant')))
        driver.find_element_by_name("Klant").clear()
        driver.find_element_by_name("Klant").send_keys(Config.sbclient)
        driver.find_element_by_xpath("//XCUIElementTypeOther[2]/XCUIElementTypeButton[1]").click()
        try:
          WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Allow')))
          driver.find_element_by_name("Allow").click()
        except TimeoutException:
          pass
        #Select English Language
        driver.find_element_by_xpath(
          "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[3]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]").click()
        # driver.find_element_by_name("DropDownArrow").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'English')))
        driver.find_element_by_name("English").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Next')))
        driver.find_element_by_name("Next").click()
        #Confirm user details
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, '2. Confirm user details')))
        driver.find_element_by_xpath("//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]").click()
        # Select team
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, '3. Choose team')))
        driver.find_element_by_name("Selecteer een team").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Team B')))
        driver.find_element_by_name("Team B").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Done')))
        driver.find_element_by_name("Done").click()


      def test_login_wizzard_teamA(self):
        driver = self.driver
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Username')))
        driver.find_element_by_name("Username").clear()
        driver.find_element_by_name("Username").send_keys(Config.username)
        driver.find_element_by_name("Password").clear()
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Password')))
        driver.find_element_by_name("Password").send_keys(Config.password)
        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.NAME, 'Client')))
        driver.find_element_by_name("Client").clear()
        driver.find_element_by_name("Client").send_keys(Config.sbclient)
        driver.find_element_by_xpath("//XCUIElementTypeOther[2]/XCUIElementTypeButton[1]").click()
        try:
          WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Allow')))
          driver.find_element_by_name("Allow").click()
        except TimeoutException:
          pass
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.NAME, 'Choose team')))
        driver.find_element_by_name("Select team").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Team A')))
        driver.find_element_by_name("Team A").click()
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'Done')))
        driver.find_element_by_name("Done").click()

      def test_logout(self):
        driver = self.driver
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'ic menuCue')))
        driver.find_element_by_name("ic menuCue").click()
        time.sleep(15)
        driver.find_element_by_name('Uitloggen').click()
        time.sleep(2)
        driver.find_element_by_name('Uitloggen').click()

      def test_logout_english(self):
        driver = self.driver
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.NAME, 'ic menuCue')))
        driver.find_element_by_name("ic menuCue").click()
        time.sleep(10)
        driver.find_element_by_name('Logout').click()
        time.sleep(2)
        driver.find_element_by_name('Logout').click()



